#ifndef _my_allocator_h_
#define _my_allocator_h_

struct header 
{
	struct header* next;
	int size;
};

typedef void* Addr;

void print_list();

/* 
	This function initializes the memory allocator and makes a portion of 
   	’_length’ bytes available. The allocator uses a ’_basic_block_size’ as 
   	its minimal unit of allocation. The function returns the amount of 
   	memory made available to the allocator. If an error occurred, 
   	it returns 0. 
*/ 
extern unsigned int init_allocator(unsigned int _basic_block_size, unsigned int _length); 

/* 
	This function returns any allocated memory to the operating system. 
   	After this function is called, any allocation fails.
*/ 
int release_allocator(); 

/* 
	Allocate _length number of bytes of free memory and returns the 
   	address of the allocated portion. Returns 0 when out of memory. 
*/ 
Addr my_malloc(unsigned int _length); 
   
/*
	Helper functions.
*/
// Combine 2 blocks.
void combine_blocks(Addr _a);
int select_index(unsigned int _length);
void to_free(struct header* new_header, int index);
int get_available_size(int index);
void remove_from_free(struct header* prev_header, int index);
   
/* 
	Frees the section of physical memory previously allocated 
   	using ’my_malloc’. Returns 0 if everything ok. 
*/
int my_free(Addr _a); 

#endif 