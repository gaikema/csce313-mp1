CC=-c -g

all: memtest

my_allocator.o : my_allocator.cpp
	g++ $(CC) my_allocator.cpp

memtest.o : memtest.cpp
	g++ $(CC) memtest.cpp

memtest: memtest.o my_allocator.o
	g++ -o memtest memtest.o my_allocator.o

.PHONY: clean
clean:
	rm -f *.o