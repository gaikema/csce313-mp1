#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include "my_allocator.hpp"

using namespace std;

int header_size = sizeof(header);
header** headers;
header* head;
int M, b;

void print_list()
{
	for (int x = 0; x <= log2(M/b); x++) 
	{
		if(headers[x])
		{
			cout << "Element " << x << ": size(" << headers[x]->size << endl;
			header* current_header = headers[x];

			while (current_header->next) 
			{
				cout << " -> size(" << current_header->size << ")" << endl;
				current_header = current_header->next;
			}
			cout << endl;
		} 
		else 
		{
			cout << "Element " << x << ": NULL" << endl;
		}
		
	}
}

unsigned int init_allocator(unsigned int _basic_block_size, unsigned int _length)
{
	M = _length;
	b = _basic_block_size;
	int prev_index = log2(M/b);
	headers = new header*[prev_index + 1]();
	head = (header*) malloc(M);

	headers[prev_index] = (struct header*) head;
	headers[prev_index]->next = NULL;
	headers[prev_index]->size = M;

	return _length-header_size;
}

int release_allocator()
{
	if(M)
	{
		free(head);
		delete(headers);
		header_size = 0;
		M = 0;
		b = 0;
	}
	
	return 0;
}

int get_available_size(int index)
{
	return b * pow(2, index) - header_size;
}

int select_index(unsigned int _length)
{
	int num_headers = log2(M/b);

	for(int i = 0; i <= num_headers; i++)
	{
		if(headers[i] && (_length <= get_available_size(i))) 
		{
			if(i == 0)	
				return i;

			if(_length <= get_available_size(i-1))
			{
				headers[i-1] = headers[i];

				if(headers[i]->next && headers[i]->next->size % 2 == 0)
				{
					headers[i] = headers[i]->next;
				}
				else
				{
					headers[i] = NULL;
				}

				char* next = (char*) headers[i - 1] + b * (int)pow(2, i - 1);
				struct header* new_header = (struct header*) next;

				new_header->next = NULL;
				new_header->size = b * (int) pow(2, i - 1);

				headers[i-1]->size = b * (int) pow(2, i - 1);
				headers[i-1]->next = new_header;

				return select_index(_length);
			}
			else
			{
				return i;
			}
		}
	}

	return -1;
}

extern Addr my_malloc(unsigned int _length) 
{
	int index = select_index(_length);	
	if (_length > M - header_size || index == -1)
	{
		cout << "Not enough space!" << endl;
		return 0;
	}

	headers[index]->size++;
	Addr result = (Addr) ((char*) headers[index] + header_size);
	if(headers[index]->next && headers[index]->next->size %2 == 0)
	{
		headers[index] = headers[index]->next;
	}
	else
	{
		headers[index] = NULL;
	}

	return result;
}

void remove_from_free(struct header* prev_header, int index)
{
	struct header* iterator = headers[index];

	if(iterator == prev_header)
	{
		if(!iterator->next)
			headers[index] = NULL;
		else
			headers[index] = iterator->next;
		return;
	}
	while(iterator->next != prev_header && iterator->next)
	{
		iterator = iterator->next;
	}
	if(!iterator->next) 
	{
		cout << "header not found!" << endl;
		return;
	}
	if(!iterator->next->next)
		iterator->next = NULL;
	else
		iterator->next = iterator->next->next;
}

void to_free(struct header* new_header, int index)
{
	new_header->next = NULL;
	if(headers[index])
	{
		struct header* iterator = headers[index];
		if(headers[index] > new_header)
		{
			new_header->next = headers[index];
			headers[index] = new_header;
			return;
		}

		while(iterator->next && iterator->next < new_header)
		{
			iterator = iterator->next;
		}

		new_header->next = iterator->next;
		iterator->next = new_header;
	}
	else
	{
		headers[index] = new_header;
	}
}

void combine_blocks(Addr _a)
{
	struct header *current_header = (struct header*) _a;
	int size = current_header->size;
	int current_index = log2(size / b);
	
	to_free(current_header, current_index);
	
	if(current_index == log2(M / b)) 
	{
		return;
	}

	int offset = (char*)current_header - (char*)head;
	int offset2 = (unsigned long long)offset ^ (unsigned long long)size;
	struct header* buddy_header = (struct header*) ((char*) head + offset2);
	
	if(buddy_header->size % 2 == 0)
	{
		remove_from_free(buddy_header, current_index);
		struct header* big_header;

		if(buddy_header < current_header)
			big_header = buddy_header;
		else
			big_header = current_header;

		big_header->size = size * 2;
		big_header->next = NULL;

		remove_from_free(current_header, current_index);
		void* start_point = (Addr)((char*)big_header + header_size);
		memset(start_point, 0, big_header->size - header_size);
		combine_blocks(big_header);
	}
	else
	{
		void* start_point = (Addr) ((char*)current_header + header_size);
		memset(start_point, 0, current_header->size - header_size);
	}
}

extern int my_free(Addr _a) 
{
	if(!_a) 
	{
		cout << "Incorrect arguments" << endl;
		return -1;
	}

	Addr adjusted_address = (Addr)((char*)_a - header_size);
	struct header *current_header = (struct header*)adjusted_address;

	if(!current_header)
	{
		cout << "Block does not exist!" << endl;
		return -1;
	}
	else if(current_header->size % 2 == 0)
	{
		cout << "This header is already freed!" << endl;
		return -1;
	} 
	else 
	{
		current_header->size--;
		combine_blocks(adjusted_address);
	}
	return 0;
}